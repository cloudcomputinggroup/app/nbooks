const express = require('express');
const app = express();
const PORT = 80;


const mysql = require('mysql')
const pool = mysql.createPool({
    host: process.env.SQL_HOST,
    user: process.env.SQL_USER,
    password: process.env.SQL_PASSWORD,
    connectionLimit: 10,
    database: "books"
})

const bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/*+json' }));

/*(req, res) => {
        res.status(200).send({

        })
    }*/
app.get('/',
    async(req, res) => {

        console.log("IT IS WORKING")
        let result;
        try {
            var s = await pool.query("SELECT title, writer FROM books",
                function (error, results, fields) {
                    if (error) {
                        console.log("Something wrong with the query");
                    }
                    console.log('RESULT: ', results);
                    console.log('RESULT: ', results[0]);
                    result = JSON.stringify(results)
                    console.log("QUERY FINISHED");
                    res.status(200).send(result);
                })
        }
        catch (e){
            res.status(404).send();
        }
    }
    );

app.post('/',
    async(req, res) => {
        console.log(req);
        console.log(req.body);
        //const post = JSON.parse(req.body);
        const rtitle = req.body.title;
        const rwriter = req.body.writer;
        var post = {title: rtitle, writer: rwriter};
        console.log(post.title)
        console.log(post.writer)
        console.log("IT IS WORKING")
        let result;
        try {
            const s = await pool.query('INSERT INTO books SET ?', post,
                function (error, results, fields) {
                    if (error) {
                        console.log("Something wrong with the query");
                    }
                    result = JSON.stringify(results)
                    console.log("HERE");
                    console.log(result);
                    res.status(200).send();
                })
        }
        catch (e){
            res.status(404).send();
        }

    }
    )

app.listen(
    PORT,
    () => console.log(`Server is running on: ...${PORT}`)
)